import React, { useEffect, useState } from "react";
import axios from "axios";
import { LazyLoadImage } from 'react-lazy-load-image-component';
import 'react-lazy-load-image-component/src/effects/blur.css';

function App() {
  const api = 'https://www.thecocktaildb.com/api/json/v1/1/search.php?f=b';
  const [data, setData] = useState([]);

  const getData = () => {
    axios.get(api)
      .then((res) => setData(res.data.drinks))
      .catch((err) => console.log(err));
  };

  useEffect(() => {
    getData();
  }, [])

  if (!data) {
    return <h1>Loading...</h1>
  }

  return (
    <div className="App">
      {data.map((item) => {
        return (
          <LazyLoadImage
            effect="blur"
            src={item.strDrinkThumb}
            alt={item.strDrink}
            key={item.idDrink}
            width="510px"
            height="510px"
          />
        );
      })}
    </div>
  );
}

export default App;
